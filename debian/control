Source: libgstreamer-perl
Section: perl
Priority: optional
Build-Depends: debhelper (>= 9), perl,
 gstreamer0.10-alsa [linux-any],
 gstreamer0.10-plugins-base,
 libextutils-depends-perl,
 libextutils-pkgconfig-perl,
 libglib-perl,
 libgstreamer0.10-dev (>= 0.10.33)
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Antonio Radici <antonio@dyne.org>, Tim Retout <diocles@debian.org>,
 Dominic Hargreaves <dom@earth.li>
Standards-Version: 3.9.4
Homepage: https://metacpan.org/release/GStreamer
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libgstreamer-perl.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libgstreamer-perl.git

Package: libgstreamer-perl
Architecture: any
Depends: ${perl:Depends}, ${shlibs:Depends}, ${misc:Depends}, libglib-perl
Recommends: gstreamer0.10-plugins-base, gstreamer0.10-alsa [linux-any]
Description: Perl interface to the GStreamer media processing framework
 GStreamer provides the means to play, stream, and convert nearly any type 
 of media -- be it audio or video. GStreamer wraps the GStreamer library in 
 a nice and Perlish way, freeing the programmer from any memory management 
 and object casting hassles.
 .
 GStreamer is a media processing framework with support for a wide variety
 of data sources, sinks, and formats through the use of dynamically loaded
 plugins. Find out more about GStreamer at http://www.gstreamer.net.
